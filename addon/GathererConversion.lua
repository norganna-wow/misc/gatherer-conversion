--[[
	GathererConversion
	
	Converts MapNotesGathering datapoints into Gatherer datapoints
	$Id$
	Version: <%version%>
]]--

GathererConversion_Version = "<%version%>";
if (GathererConversion_Version == "<".."%version%>") then
	GathererConversion_Version = "1.0.DEV";
end

local function Print(message)
	ChatFrame1:AddMessage("GathererConversion: "..message, .95, .95, .95);
end

function GathererConversion_OnLoad()
	Print("GathererConversion v"..GathererConversion_Version);
	Print("Use /gconvert to convert MapNotesGathering data to Gatherer data");
	
	SLASH_GCONVERT1 = "/gconvert";
	SlashCmdList["GCONVERT"] = GathererConversion_ChatCommandHandler;
end

function GathererConversion_ChatCommandHandler(msg)
	Print("Converting MapNotesGathering data to Gathering data");

	Print("Clearing Gathering Data");
	GatherItems = {}

	for continent, zones in MapNotesGathering_Data do
		Print("Converting continent: "..GatherRegionData[continent][0].name);
		GatherItems[continent] = {}
		
		for zone, data in zones do
			local zoneInfo = GatherRegionData[continent][zone];
			if (zoneInfo) then
				Print("Converting zone: "..zoneInfo.name);

				GatherItems[continent][zone] = {}
	
				for item, itemData in data do
					local icon = itemData["icon"];
					local itemIndex = itemData["subname"];
					local itemName = string.lower(MapNotesGathering_Names[icon][itemIndex])
					
					local x = itemData["xPos"] * 100;
					local y = itemData["yPos"] * 100;
					local absX, absY = Gatherer_AbsCoord(continent, zone, x, y);
					
					local gatherIcon, gatherType;
					if ((icon >= 1) and (icon <= 9)) then
						gatherIcon = Gatherer_FindOreType(itemName);
						gatherType = "Ore";
					elseif ((icon >= 10) and (icon <= 38)) then
						gatherIcon = itemName;
						gatherType = "Herb";
					elseif (icon >= 39) then
						gatherIcon, itemName = Gatherer_FindTreasureType(itemName);
						gatherType = "Treasure";
					else
						Sea.IO.print("Unknown Item: "..itemName);
						itemName = nil;
						gatherIcon = "";
						gatherType = "";
					end				
	
					if ( itemName ) then
						if ( not GatherItems[continent][zone][itemName] ) then
							GatherItems[continent][zone][itemName] = {};
						end
	
						-- Insert our converted item now				
						table.insert(GatherItems[continent][zone][itemName],
							{
								["x"] = math.floor(x * 100)/100,
								["y"] = math.floor(y * 100)/100,
								["count"] = 1,
								["icon"] = Gatherer_GetDB_IconIndex(gatherIcon, gatherType),
								["gtype"] = Gather_DB_TypeIndex[gatherType],
							} );
					end
				end
			else
				Print("Skipping unknown zone: "..zone);
			end
		end
	end
	Print("Gatherer data conversion completed.");
	
end
