## Interface: 1600
## Title: GathererConversion
## Notes: Converts MapNotesGathering datapoints into Gatherer datapoints.
## Author: CheshireKatt
## Version: <%version%>
## Dependencies: MapNotesGathering, Gatherer
GathererConversion.xml
