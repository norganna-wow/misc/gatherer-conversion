<?
// $Id$

include_once("versions/GathererConversion.php");

// The current release version. (check the webpage)
$release = "1.0.0007";

?>
<h3>GathererConversion</h3>

<p>GathererConversion is an addon written by CheshireKatt that allows users of MapNotes-Gatherer to import their pre-existing MapNotes and convert them to Gatherer format.

<p>Please note that while everything is expected to go smoothly, this can still be risky, and it is advised to first make a backup of your SavedVariables.lua file (which can be found in <b>World of Warcraft\WTF\Account\<i>Your Account</i>\SavedVariables.lua</b>)

<p>Please note: When you convert your MapNotes data, all your existing Gatherer data will be overwritten.

<h3>Download</h3>

<p>The following revisions are available for download:<br>
<?

print "<a href=\"versions/$name-$release.zip\">$name-$release.zip</a> - Current Release Version ($release)<br/>\n";
if ($headversion != $release) {
	print "<a href=\"versions/$headfile.zip\">$headfile.zip</a> - Current Development Version ($headversion)<br/>\n";
	print "Note: The latest development version may not be suitable for use. Try it at your own risk.<br/>";
}

